// print hello world in java
System.out.println("Hello World");

// Java program to check for palindrome
// or not
import java.util.Scanner;

class Palindrome
{
    // Function to check palindrome
    static boolean isPalindrome(String str)
    {
        // Convert given string
        // to character array
        char[] strArray = str.toCharArray();

        // Pointers pointing to the beginning
        // and the end of the array
        int i = 0;
        int j = strArray.length - 1;
        while (j > i) {
            if (strArray[i] != strArray[j]) {
                return false;
                        }
            i++;
            j--;
        }
        return true;

                }

    // Driver code
    public static void main(String[] args)
    {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        if (isPindrome(str))
            System.out.println("Yes");
        else
            System.out.println("No");
                }
}   